# -*- coding: utf-8 -*-
from sympy import *
import math


print "Função: (x^5 - e^x) | Erro: 0.00001%"

x0 = float(raw_input("Informe o valor de x0: "))

erro = 0.00001
erro_relativo_percentual = 1

'''Essa parte possui a função e realiza a derivada dela'''
x = Symbol('x')
funcao = (x ** 5) - exp(x)
derivada = funcao.diff(x)

n=0
print "k\t|\tx_k-1\t|\tx_k\t|   f(x_k)\t|   f'(x_k)\t|   erro rel\t|   erro rel percentual"
while (erro_relativo_percentual > erro):
	
	if n==0:
		f_x0 = round(funcao.evalf(subs={x: x0}), 7)
		f_linha_x0 = round(derivada.evalf(subs={x: x0}), 7)
		print "%d\t|  -------\t|  %.7f\t|  %.7f\t|  %.7f\t|  -------\t|  -------" % (n, x0, f_x0, f_linha_x0)

	n=n+1
	x1 = round(round(x0, 7) - round(funcao.evalf(subs={x: x0}), 7) / round(derivada.evalf(subs={x: x0}), 7), 7)
	f_x1 = round(funcao.evalf(subs={x: x1}), 7)
	f_linha_x1 = round(derivada.evalf(subs={x: x1}), 7)
	erro_abs = round(abs(x1-x0), 7)
	erro_rel = round((abs(erro_abs) / abs(x1)), 7)
	erro_relativo_percentual = round((erro_rel * 100), 5)
	print "%d\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.5f" % (n, x0, x1, f_x1, f_linha_x1, erro_rel, erro_relativo_percentual)
	
	x0 = x1

print "\n"
print "O número total de iterações é %d" % n