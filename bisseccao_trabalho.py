# -*- coding: utf-8 -*-
from sympy import *
import math
import numpy as np


print "Função: (x^5 - e^x) | Erro: 0.00001%"

print "Dado o intervalo [a,b]: "

a = float(raw_input("Informe o valor de a: "))
b = float(raw_input("Informe o valor de b: "))

erro = 0.00001
erro_relativo_percentual = 1

x = Symbol('x')
funcao = (x ** 5) - exp(x)

n=0
fa = round(funcao.evalf(subs={x: a}), 7)
fb = round(funcao.evalf(subs={x: b}), 7)

if fa * fb < 0:
	print "k\t|\ta\t|\tb\t|\tx_k\t|\tf(a)\t|\tf(b)\t|\tf(x_k)\t|   erro rel\t|   erro rel percentual"
	while (erro_relativo_percentual > erro):
		fa = round(funcao.evalf(subs={x: a}), 7)
		fb = round(funcao.evalf(subs={x: b}), 7)
	
		m = round(((a+b) / 2), 7)
		fm = round(funcao.evalf(subs={x: m}), 7)
		
		#salva o valore antigo de a e b
		old_a = a 
		old_b = b 

		if fm * fa < 0:
			b=m
		else:
			a=m
		erro_abs = round(abs(old_b-old_a), 7)
		if old_b == 0:
			erro_rel = round(erro_abs, 7)
			erro_relativo_percentual = round(erro_rel, 7)
			print "%d\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.5f" % (n, old_a, old_b, m, erro_abs, erro_rel, erro_relativo_percentual)
		else:
			erro_rel = round((abs(erro_abs) / abs(old_b)), 7)
			erro_relativo_percentual = round((erro_rel * 100), 5)
			print "%d\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.7f\t|  %.5f" % (n, old_a, old_b, m, fa, fb, fm, erro_rel, erro_relativo_percentual)
		n = n+1

	print "\n O número total de iterações é %d" % (n-1)
elif fa * fb == 0:
	if fa == 0:
		print "\n A raíz é %f" % a    #no caso, se o valor de a do intervalo já for a raiz
	else:
		print "\n A raíz é %f" % b    #no caso, se o valor de b do intervalo já for a raiz
else:
	print "\n Função não possui raíz nesse intervalo!"
	print "\n O número total de iterações é %d" % (n)
